package fr.afpa.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jboss.jandex.Main;
import fr.afpa.beans.Personne;
import fr.afpa.sessions.HibernateUtils;

public class VerifLogDAO {
	private Session s;

	public Personne verifLog(String login, String password) {
		Session session = HibernateUtils.getSession();
	// le nom de NamedQuery qui est sur la classe de recuperation 
	// ici sur la classe Personne
//		Query q = session.getNamedQuery("VerifPersonne");
//		q.setParameter("login", login);
//		q.setParameter("password", password);
//		
		Query q = session.getNamedQuery("FindByLogin");
		q.setParameter("login", login);
		try {
			return (Personne) q.getSingleResult();
		} catch (NoResultException e) {
			// Personne pas en BDD
			return null;
		} finally {
			// Ferme la session dans tous les cas
			session.close();
		}
		
	}

}
