package fr.afpa.dao;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import fr.afpa.beans.Personne;
import fr.afpa.sessions.HibernateUtils;

public class GestionUtilisateurDAO {
	public boolean verifUser(Personne personne) {
	
		Session s = null;
		//Creation d une session hibernate
		s= HibernateUtils.getSession();
		//Debut de la transaction
		Transaction tx=s.beginTransaction();
		
		s.save(personne);
		tx.commit();
		return true;
	}	
	
	public Personne findByLogin(String login) {
		Session session = HibernateUtils.getSession();
		// le nom de NamedQuery qui est sur la classe de recuperation 
		// ici sur la classe Personne
		// session
		Query q = session.getNamedQuery("FindByLogin");
		q.setParameter("login", login);
			
		try {
			return (Personne) q.getSingleResult();
		} catch (NoResultException e) {
			// Personne pas en BDD
			return null;
		} finally {
			// Ferme la session dans tous les cas
			session.close();
		}
		
			
	}
		
	public Personne modifPersonne(Personne personne,String login) {
		
		Session session = HibernateUtils.getSession();

		//Debut de la transaction
		Transaction tx=session.beginTransaction();
		
		Query q = session.getNamedQuery("FindByLogin");
		q.setParameter("login", login);

		
		try {
		Personne entite = (Personne) q.getSingleResult();
		entite.setLogin(entite.getLogin());
		entite.setPassword(entite.getPassword());
		entite.setNom(entite.getNom());
		entite.setPrenom(entite.getPrenom());
		entite.setNomLibraire(entite.getNomLibraire());
		entite.setAdresse(entite.getAdresse());
		entite.setEmail(entite.getEmail());
		entite.setTel(entite.getTel());
		
		session.merge(personne);
		tx.commit();
		return entite;
		
		
		
		} catch (NoResultException e) {
			
			return null;
		} finally {
			// Ferme la session dans tous les cas
			session.close();
		}
		
	}	
}
