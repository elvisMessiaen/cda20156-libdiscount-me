package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;

import com.sun.istack.NotNull;

import lombok.Data;
import lombok.ToString;

@Data 
@ToString
@Entity
		
@NamedQuery(name="VerifPersonne", query = "SELECT e FROM Personne e WHERE e.login=:login and e.password=:password")
@NamedQuery(name="FindByLogin", query = "SELECT e FROM Personne e WHERE e.login=:login")
public class Personne {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private long idPerson;
	@Column(nullable = false)
    private String login;
    @Column(nullable = false)
    private String password;
	@Column
	private String nom;
	@Column
	private String prenom;
	@Column
	private String nomLibraire;
	@Column
	private String adresse;
	@Column
	private String email;
	@Column(name ="telephone")
	private String tel;

}

