package fr.afpa.authentification;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Personne;
import fr.afpa.dao.GestionUtilisateurDAO;
import fr.afpa.dao.VerifLogDAO;

public class Modif extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Get current logged user
		// Set session login
		RequestDispatcher disp = null;
		String nextVue = "modifCompte.jsp";
		HttpSession session = request.getSession();
		String login = (String) session.getAttribute("login");

		
		Personne personne = (Personne) new GestionUtilisateurDAO().findByLogin(login);
		request.setAttribute("personne", personne);
		disp = request.getRequestDispatcher(nextVue);
		disp.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Personne par = new Personne();
		HttpSession session = request.getSession();
		String login = (String) session.getAttribute("login");


		par.setLogin(request.getParameter("login"));
		par.setPassword(request.getParameter("password"));
		par.setNom(request.getParameter("nom"));
		par.setPrenom(request.getParameter("prenom"));
		par.setNomLibraire(request.getParameter("nomLibraire"));
		par.setAdresse(request.getParameter("adresse"));
		par.setEmail(request.getParameter("email"));
		par.setTel((request.getParameter("tel")));
		

		RequestDispatcher disp = null;
		String nextVue = "modifCompte.jsp";

		if (!par.getLogin().isEmpty() || !par.getPassword().isEmpty() || !par.getNom().isEmpty()
				|| !par.getPrenom().isEmpty() || !par.getNomLibraire().isEmpty() || !par.getAdresse().isEmpty()
				|| !par.getEmail().isEmpty() || par.getTel().isEmpty()) {

			request.setAttribute("nom", par.getNom());
			nextVue = "accueil.jsp";
			par = new GestionUtilisateurDAO().modifPersonne(par, login);
		} else {
			System.out.println(" tous les champs doivent tous être remplis");
			nextVue = "creatCompte.jsp";
		}

		disp = request.getRequestDispatcher(nextVue);
		disp.forward(request, response);

	}
}
