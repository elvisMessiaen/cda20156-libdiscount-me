package fr.afpa.authentification;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Personne;
import fr.afpa.dao.GestionUtilisateurDAO;

/**
 * Servlet implementation class Creation
 */
public class Creation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// R�cup�ration des informations saisies par l'utilisateur
		Personne personne = new Personne();
		personne.setLogin(request.getParameter("login"));
		personne.setPassword(request.getParameter("password"));
		personne.setNom(request.getParameter("nom"));
		personne.setPrenom(request.getParameter("prenom"));
		personne.setNomLibraire(request.getParameter("nomLibraire"));
		personne.setAdresse(request.getParameter("adresse"));
		personne.setEmail(request.getParameter("email"));
		personne.setTel((request.getParameter("tel")));
		

		RequestDispatcher disp = null;
		String nextVue = "creatCompte.jsp";

		if (!personne.getLogin().isEmpty() || !personne.getPassword().isEmpty() || !personne.getNom().isEmpty()|| !personne.getPrenom().isEmpty() || 
				!personne.getNomLibraire().isEmpty() || !personne.getAdresse().isEmpty()|| !personne.getEmail().isEmpty()|| personne.getTel().isEmpty()) {
			GestionUtilisateurDAO per = new GestionUtilisateurDAO();		
			per.verifUser(personne);
			
			// Set session login
			HttpSession session = request.getSession();
	        session.setAttribute("login", personne.getLogin());
	        
			request.setAttribute("nom",personne.getNom());
			nextVue = "accueil.jsp";
		} else {
			System.out.println(" tous les champs doivent tous être remplis");
			nextVue = "creatCompte.jsp";
		}

		disp = request.getRequestDispatcher(nextVue);
		disp.forward(request, response);

	}
}


