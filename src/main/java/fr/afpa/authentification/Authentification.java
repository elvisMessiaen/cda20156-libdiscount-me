package fr.afpa.authentification;

import java.io.IOException;

import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Personne;
import fr.afpa.dao.VerifLogDAO;
import fr.afpa.sessions.HibernateUtils;

public class Authentification extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher disp = null;
		String nextVue;
		
		// pour la cession
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		
	
		Personne personne = (Personne) new VerifLogDAO().verifLog(login, password);
		if (personne != null) {
			System.out.println(personne);
			
			// Set session login
			HttpSession session = request.getSession();
	        session.setAttribute("login", personne.getLogin());
	        
			nextVue = "accueil.jsp";
			request.setAttribute("nom",personne.getNom());

		} else {
			System.out.println("personne pas en bdd");
			nextVue = "creatCompte.jsp";
		}

		disp = request.getRequestDispatcher(nextVue);
		disp.forward(request, response);

	}
}
