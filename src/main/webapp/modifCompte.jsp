<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="css/style.css" rel="stylesheet">
<title>Modification de compte</title>
</head>
<body>
<h1 id="titreLogin">Bienvenue sur  Sell-Book</h1>
	<div id="containerCreaCompte">
		<fieldset>
<!-- 	Veuillez remplir les champs pour un enregistrement : -->
			 <legend>Veuillez remplir les champs pour enregistrer les modifications : </legend>
			<form id="direcBoite" action="Modif" method="post" name="formuCreation">
				
				<label class="login" for="login">Pseudo :</label> 
				<input type="text" name="login" required value="${personne.login}" id="TB1" onkeyup="Autotab(2, this.size, this.value)" autofocus/> 
				
				<label class="password" for="pasword">Mot de passe :</label>
				<input type="password" name="password" required value="${personne.nom}" id="TB2" onkeyup="Autotab(3, this.size, this.value)" /> 
				
				<label class="nom" for="nom">Nom :</label> 
				<input type="text" name="nom" required value="${personne.nom}"  id="TB3" onkeyup="Autotab(4, this.size, this.value)"/> 
				
				<label class="prenom" for="prenom">Prenom :</label> 
				<input type="text" name="prenom" required value="${personne.prenom}" id="TB4" onkeyup="Autotab(4, this.size, this.value)"/> 
				
				<label class="nomLibraire" for="nom">Nom de votre librairie :</label> 
				<input type="text" name="nomLibraire" required value="${personne.nom}" id="TB5" onkeyup="Autotab(5, this.size, this.value)"/> 
				
				<label class="adresse" for="adresse">Adresse de la librairie :</label> 
				<input type="text" name="adresse"  required  value="${personne.nomLibraire}" id="TB6" onkeyup="Autotab(6, this.size, this.value)"/> 
				
				<label class="email" for="email">E-mail :</label> 
				<input type="email" name="email" required value="${personne.email}" id="TB7" onkeyup="Autotab(7, this.size, this.value)"/> 
				
				<label class="telephone" for="telephone">Telephone :</label> 
				<input type="tel" id="tel" name="tel" pattern="[0-9]{10}" required value="${personne.tel}" id="TB8" onkeyup="Autotab(8, this.size, this.value)">
				
			    <input id="submi" type="submit" value="connexion" id="TB1" onkeyup="Autotab(8, this.size, this.value)"> 
		</fieldset>		
			</form>
	</div>
	<script type="text/javascript" src="monscript.js"></script>
</body>
</html>